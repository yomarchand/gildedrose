const {Shop, Item} = require("../src/gilded_rose");
const expect_result = require("./expect_result")

describe("Gilded Rose", function() {
  it("should foo", function() {
    const gildedRose = new Shop([new Item("fixme", 0, 0)]);
    const items = gildedRose.updateItem();
    expect(items[0].name).toBe("fixme");
  });
});

describe("Update Quality", () => {
    const items = [
      new Item("+5 Dexterity Vest", 10, 20),
      new Item("Aged Brie", 2, 0),
      new Item("Elixir of the Mongoose", 5, 7),
      new Item("Sulfuras, Hand of Ragnaros", 0, 80),
      new Item("Sulfuras, Hand of Ragnaros", -1, 80),
      new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
      new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
      new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),

      // This Conjured item does not work properly yet
      new Item("Conjured Mana Cake", 3, 6),
    ];

    const days = Number(process.argv[2]) || 2;
    const gildedRose = new Shop(items);

    for (var day = 0; day < days; day++) {
      var i = 0;
      items.forEach(item => {
          expect(item.sellIn).toBe(expect_result[day][i].sellIn)
          expect(item.quality).toBe(expect_result[day][i].quality)
          i++
        })
      gildedRose.updateItem();
    }
})

