module.exports =
    [
        [
            {
                sellIn: 10,
                quality: 20,
            },

            {
                sellIn: 2,
                quality: 0,
            },
            {
                sellIn: 5,
                quality: 7,
            },

            {
                sellIn: 0,
                quality: 80,
            },

            {
                sellIn: -1,
                quality: 80,
            },

            {
                sellIn: 15,
                quality: 20,
            },

            {
                sellIn: 10,
                quality: 49,
            },

            {
                sellIn: 5,
                quality: 49,
            },

            {
                sellIn: 3,
                quality: 6,
            }
        ],

        [
            {
                sellIn: 9,
                quality: 19,
            },

            {
                sellIn: 1,
                quality: 1,
            },
            {
                sellIn: 4,
                quality: 6,
            },

            {
                sellIn: 0,
                quality: 80,
            },

            {
                sellIn: -1,
                quality: 80,
            },

            {
                sellIn: 14,
                quality: 21,
            },

            {
                sellIn: 9,
                quality: 50,
            },

            {
                sellIn: 4,
                quality: 50,
            },

            {
                sellIn: 2,
                quality: 5,
            }
        ]
    ]