étapes:


1 • Améliorer le test coverage -> créer plusieurs tests
2 • Déplacer les constantes dans un fichier à part (les conditions)

3 • Diviser la fonction updateQuality en deux : updateQuality(), updateSellIn()
4 • Déplacer la fonction updateQuality() dans Item au lieu de Shop -> éviter les conditions à rallonge
5 • Créer des entités pour chaque type d'objet -> de lutter contre les else if dans le fichier gilded_rose.js



Utiliser les texttest_fixture.js pour réaliser des tests unitaires dans gilded_rose.test.js


# Gilded Rose

This is the Gilded Rose kata in JavaScript with Jest

## Getting started

Install dependencies

```sh
npm install
```

## Running tests

To run all tests

```sh
npm test
```

To run all tests in watch mode

```sh
npm run test:watch
```

To generate test coverage report

```sh
npm run test:coverage
```
