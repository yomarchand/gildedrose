const {MIN_SELLIN} = require("./item_consts");

module.exports = {
    name: 'Aged Brie',
    update: (item) => {
        item.increaseQuality()

        if (item.sellIn < MIN_SELLIN) {
            item.increaseQuality();
        }
    }
}