const {
    BACKSTAGE_PASSES_FIRST_STEP,
    BACKSTAGE_PASSES_SECOND_STEP,
    MIN_SELLIN,
    MIN_QUALITY,
    MAX_QUALITY
} = require('./item_consts')

module.exports = {
    name : "Backstage",
    update: function(item) {
        if (item.sellIn < MIN_SELLIN) {
            item.quality = MIN_QUALITY;
            return item;
        }
        
        item.increaseQuality();

        if (item.sellIn < BACKSTAGE_PASSES_FIRST_STEP) {
            item.increaseQuality();
        }

        if (item.sellIn < BACKSTAGE_PASSES_SECOND_STEP) {
            item.increaseQuality();
        }

        return item;
    }
}