const {
  MIN_QUALITY,
  MAX_QUALITY,
} = require('./item_consts')

const sulfura  = require('./sulfura')
const backstage = require('./backstage')
const agedBrie = require('./agedBrie')

class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }

  updateSellIn() {
    if (!this.name.includes(sulfura.name)) {
      this.sellIn--;
    }
  }

  updateQuality() {
    if (this.name.includes(sulfura.name)) {
      sulfura.update(this)
    } else if (this.name.includes(backstage.name)) {
      backstage.update(this)
    } else if (this.name.includes(agedBrie.name)){
      agedBrie.update(this)
    } else {
      this.decreaseQuality()
    }
  }
  
  increaseQuality() {
    if (this.quality < MAX_QUALITY) {
      this.quality++;
    }
  }

  decreaseQuality() {
    if (this.quality > MIN_QUALITY) {
      this.quality--
    }
  }
}

class Shop {
  constructor(items=[]){
    this.items = items;
  }

  updateItem() {
    for (let i = 0; i < this.items.length; i++) {
      this.items[i].updateQuality()
      this.items[i].updateSellIn()
    }

    return this.items;
  }
}

module.exports = {
  Item,
  Shop
}
